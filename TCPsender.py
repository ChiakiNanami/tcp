from http import client
from pydoc import cli
from socket import *
import json
import time
from dataclasses import dataclass, field
from typing import Generator, List, Optional, Tuple, Set
import random
from sqlalchemy import true
from threading import Thread
import pickle




class timer:
    def __init__(self):
        self.timeLimit = 0
        self.startTime = 0
        self.isAlive = False
    
    def startTimer(self, seconds):
        self.startTime = time.time()
        self.timeLimit = seconds
        self.isAlive = True
    
    def stopTimer(self):
        self.isAlive = False

    def getTime(self):
        return time.time() - self.startTime

    def isTimeout(self):
        return self.isAlive and time.time() > self.startTime + self.timeLimit

def make_pkt(seq= -1, ack=-1, syn=False, fin=False, data='', rwnd=5):
    sendData = {}
    sendData['seq'] = seq
    sendData['ack'] = ack
    sendData['syn'] = syn
    sendData['fin'] = fin
    sendData['data'] = data
    sendData['rwnd'] = rwnd
    sendMessage = json.dumps(sendData)
    return sendMessage


@dataclass
class tcpSender:
    def __init__(self):
        self.synRetries: int = 5
        self.tcpRetries: int = 10
        self.messageLength: int = 0
        self.sampleRTT: float = 0
        self.EstimatedRTT: float = 0.03
        self.DevRTT: float = 0.03
        self.TimeoutInterval: float = 0.05
        # waiting: set = field(default_factory=lambda: set())
        self.resend: set = set()
        self.sendBase: int = 0
        self.nextSendNum: int = 0
        # listenFlag: bool = False
        self.duplicateNum: int = 0
        self.block: bool =  False
        self.toResend: bool = False
        self.endFlag: bool = False #用于终止TCP连接
        self.timer: timer = timer()
        self.rwnd2: int = 10
        self.cacheSize: int = 20
        self.cache: list = [None]*self.cacheSize
        self.app: list = []
        self.toAccept: int = 0
        self.messages: str = ''
        self.left: int = 0
        self.address2: tuple = ('localhost', 12000)
        self.clientSocket = socket(AF_INET, SOCK_DGRAM)
        self.clientSocket.setblocking(False) # 非阻塞
        self.finitedMsgLen = 0

    def shakeHands(self) -> bool:
        clientIsn = random.randint(1, 1024)
        serverIsn = -1
        SYNPKT = make_pkt(seq=clientIsn, syn=True).encode()
        timeLim = self.TimeoutInterval
        tryTimes = 0
        while tryTimes < self.synRetries:
            print('sending', SYNPKT)
            self.clientSocket.sendto(SYNPKT,self.address2)
            startTime = time.time()
            while time.time() < startTime + timeLim:
                try:
                    rcvpkt = self.clientSocket.recv(1024)
                    rcvpkt = json.loads(rcvpkt.decode())
                    if rcvpkt['syn'] == True and rcvpkt['ack'] == clientIsn + 1:
                        serverIsn = rcvpkt['seq']
                        break
                    else:
                        return False
                except BlockingIOError: #服务器尚未回复
                    # print("Waiting SYNACK")
                    time.sleep(.01)
                    continue
                except ConnectionResetError: #服务器未开启或存在故障
                    print('Link lost')
                    return False
            if serverIsn != -1:
                break
            print('timeout, resend SYN')
            timeLim *= 2
            tryTimes += 1

        if tryTimes == self.synRetries:
            print("No response")
            return False
        pkt2 = make_pkt(seq=clientIsn+1, ack=serverIsn +1).encode()
        print('sending', pkt2)
        self.clientSocket.sendto(pkt2, self.address2)
        return True

    def endTCP(self, recv: bool):
        self.endFlag = True
        finseq = random.randint(1, 1024)
        finpkt = make_pkt(fin=True, seq=finseq).encode()
        timeLim = self.TimeoutInterval
        self.timer.startTimer(timeLim)
        self.clientSocket.sendto(finpkt, self.address2)
        print('send', finpkt)
        tries = 0
        while True:
            if self.timer.isTimeout():
                tries += 1
                if tries == self.synRetries:
                    return
                timeLim *= 2
                self.timer.startTimer(timeLim)
                self.clientSocket.sendto(finpkt, self.address2)
            try:
                rcvpkt = json.loads(self.clientSocket.recv(1024).decode())
                if rcvpkt['ack'] == finseq + 1:
                    break
                if recv and rcvpkt['fin']:
                    pkt = make_pkt(ack=rcvpkt['seq']+1).encode()
                    self.clientSocket.sendto(pkt, self.address2)
            except BlockingIOError:
                pass
            except ConnectionResetError:
                return
        if not recv:
            while True:
                try:
                    rcvpkt = json.loads(self.clientSocket.recv(1024).decode())
                    if rcvpkt['fin']:
                        pkt = make_pkt(ack=rcvpkt['seq']+1).encode()
                        self.clientSocket.sendto(pkt, self.address2)
                        print('send', pkt)
                        break
                except BlockingIOError:
                    pass
                except ConnectionResetError:
                    return



    
    def zeroDetect(self):
        tries = 0
        detectpkt = make_pkt(seq=-2).encode() #零窗口探测分组
        timeLim = self.TimeoutInterval
        while self.block:
            time.sleep(timeLim)
            self.clientSocket.sendto(detectpkt, self.address2)
            # print('zero detect')
            timeLim = timeLim*2 if timeLim < 30 else 60
            tries += 1
            if tries == self.tcpRetries:
                self.endFlag = True
                print('server no response, link ends')
                return
        self.timer.startTimer(self.TimeoutInterval)



    def send(self, messages):
        self.timer.startTimer(self.TimeoutInterval)
        pkt = None
        while not self.endFlag and self.sendBase < self.messageLength:
            if self.block:
                self.zeroDetect()
                continue
            if self.toResend:
                pkt = make_pkt(seq=self.sendBase, data=messages[self.sendBase-self.finitedMsgLen], rwnd=self.cacheSize+self.left-self.maxRecv-1).encode()
                self.resend.add(self.sendBase)
                self.toResend = False
                self.clientSocket.sendto(pkt, self.address2)
                # print("resend: ", pkt)
            elif self.nextSendNum < self.messageLength and self.sendBase > self.nextSendNum - self.rwnd2:
                pkt = make_pkt(seq=self.nextSendNum, data=messages[self.nextSendNum-self.finitedMsgLen], rwnd=self.cacheSize+self.left-self.maxRecv-1).encode()
                self.nextSendNum += 1
                # print(self.nextSendNum)
                self.clientSocket.sendto(pkt, self.address2)
                # print("Send: ", pkt)

            time.sleep(.01)
        self.timer.stopTimer()
        # self.messages = '' #下次send时会重新初始化
        # self.messageLength = 0
        self.finitedMsgLen = self.messageLength

    def use(self): #模拟应用层从缓存中读取数据
        temp = 0
        while not self.endFlag:
            # print(self.cache)
            if self.cache[temp] != None:
                self.app.append(self.cache[temp])
                self.cache[temp] = None
                self.left += 1
                # print('left', self.left)
                temp = (temp+1)%self.cacheSize
                if self.app[-1] == '\n':
                    print(''.join(self.app))
                    self.app.clear()
                    # self.toAccept = 0
                    # self.left = 0
                    # self.maxRecv = 0
                    # temp = 0
            time.sleep(.3)


    def listen(self):
        tries = 0
        timeLim = self.TimeoutInterval
        self.maxRecv = 0
        while not self.endFlag:
            if self.timer.isTimeout():
                # print('timeout')
                self.toResend = True #resend
                timeLim *= 2
                self.timer.startTimer(timeLim)
                tries += 1
                if tries == self.tcpRetries:
                    self.endFlag = True #终止连接
                    print('server no response, link ends')
                    return

            try:
                rcvpkt = self.clientSocket.recv(1024)
                if random.random() < 0.25: #模拟丢包
                    continue
                rcvpkt = json.loads(rcvpkt.decode())
                if rcvpkt['syn']:
                    pkt = make_pkt(ack=rcvpkt['seq'] +1).encode()
                    self.clientSocket.sendto(pkt, self.address2)
                if rcvpkt['fin']:
                    pkt = make_pkt(ack=rcvpkt['seq']+1).encode()
                    self.clientSocket.sendto(pkt, self.address2)
                    self.endTCP(True)
                    break
                # print("Receive: ", rcvpkt)
                tries = 0
                self.rwnd2 = rcvpkt['rwnd']
                if rcvpkt['rwnd'] <= 0:
                    self.block = True
                    self.timer.stopTimer()
                else:
                    self.block = False
                # print('zero')
                if rcvpkt['seq'] == -2: #收到零窗口探测分组
                    # message = self.messages[self.sendBase] if self.messages else ''
                    ackpkt = make_pkt(ack=self.toAccept, rwnd=self.cacheSize+self.left-self.maxRecv-1).encode()
                    self.clientSocket.sendto(ackpkt, self.address2)
                    # print('send', ackpkt)
                    continue
                # print('data')
                if rcvpkt['seq'] < self.left + self.cacheSize and rcvpkt['seq'] >=  0:
                    #收到数据分组
                    # print('data!')
                    if rcvpkt['seq'] >= self.toAccept: #收到期望的数据分组
                        self.maxRecv = max(rcvpkt['seq'], self.maxRecv)
                        # print('left',self.left)
                        self.cache[rcvpkt['seq']%self.cacheSize] = rcvpkt['data']
                        if rcvpkt['seq'] == self.toAccept:
                            while self.toAccept < self.left + self.cacheSize and self.cache[self.toAccept%self.cacheSize]:
                                self.toAccept += 1
                            # print(self.toAccept)
                        ackpkt = make_pkt(ack=self.toAccept, rwnd=self.cacheSize+self.left-self.maxRecv-1).encode()
                        self.clientSocket.sendto(ackpkt, self.address2)
                        # print('send', ackpkt)
                        continue
                    else: #收到重复的数据分组
                        ackpkt = make_pkt(ack=self.toAccept, rwnd=self.cacheSize+self.left-self.maxRecv-1).encode()
                        self.clientSocket.sendto(ackpkt, self.address2)
                        # print('send', ackpkt)
                
                #收到ack分组
                # print('ACK?')
                if rcvpkt['ack'] > self.sendBase: 
                    # print('ACK!')
                    self.toResend = False #收到非冗余ack，不需要重传（或终止重传）
                    if self.sendBase not in self.resend:
                        self.sampleRTT = self.timer.getTime()
                        self.EstimatedRTT = 0.875 * self.EstimatedRTT + 0.125 * self.sampleRTT
                        self.DevRTT = 0.75 * self.DevRTT + 0.25 * abs(self.EstimatedRTT - self.sampleRTT)
                        self.TimeoutInterval = self.EstimatedRTT + 4 * self.DevRTT
                        # print(self.TimeoutInterval)
                        self.resend.clear()

                    self.sendBase = rcvpkt['ack']
                    timeLim = self.TimeoutInterval
                    self.timer.startTimer(timeLim)
                    self.duplicateNum = 0
                elif rcvpkt['ack'] == self.sendBase:
                    self.duplicateNum += 1
                    if self.duplicateNum == 3: #连续收到3个冗余ack，重传
                        #resend
                        # print("three duplicate ACK")
                        self.toResend = True
                        self.duplicateNum = 0
            
            except BlockingIOError:
                # print(e)
                time.sleep(0.01)
                continue

    def run(self):
        # self.seqLength = 20
        # self.listenFlag = True
        if self.shakeHands(): #如果握手成功
            Thread(target=self.listen).start()
            Thread(target=self.use).start()
            while not self.endFlag:
                messages = input("Input sentence: \n")
                if self.endFlag:
                    return
                if messages == "/quit":
                    self.endTCP(0)
                    return
                messages = messages + '\n'
                self.messageLength += len(messages)
                self.send(messages)
        




if __name__=='__main__':
    s = tcpSender()
    s.run()
    # print(s.shakeHands())